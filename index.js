const http = require('http');
var express = require('express');
var app = express();
var path = require('path');
const PORT = process.env.PORT || 8080;

app.use(express.static(__dirname + '/'));
// viewed at http://localhost:8080
app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
    console.log('loaded')
});
app.get('/guia-estilos', function(req, res) {
    res.sendFile(path.join(__dirname + '/guia-estilos.html'));
    console.log('loaded')
});

app.listen(PORT);
