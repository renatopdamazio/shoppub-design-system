$('.mini-carousel').slick();

//Submenu functions.
$('.has-submenu').click(function() {
  $(this)
    .toggleClass('mobile-active-link')
    .next('ul')
    .toggleClass('mobile-active-submenu')
});

$('.toggle-mobile-nav').click(function() {
  $('html')
    .toggleClass('show-menu-mobile-nav');
});

//Banner home
$('#banner-home').slick({
  adaptiveHeight: true,
  dots: true
});

//Band Communique.
$('.band-comunique').slick({
  slidesToShow: 4,
  dots: false,
  arrow: true,
  infinite: false,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: false
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        infinite: false
      }
    }
  ]
});

//Keep Menu fixed.
//Hide Header on on scroll down
var didScroll;
var lastScrollTop = 0;
var delta = 5;
var header = $('header[role="header"]')
var navbarHeight = header.outerHeight();

function toggleCartPreview() {
  $('html').toggleClass('show-cart-preview')
}

if ($(window).width() > 960) {
  $('.toogleCartPreview').click(function() {
    toggleCartPreview()
  })
}


if ($(window).width()) {
  if (!header.hasClass('fixed-header')) {

    header.addClass('fixed-header');
    $('body').attr('style', 'padding-top:' + navbarHeight + 'px');

  }
} else {
  if (header.hasClass('fixed-header')) {
    header.removeClas('fixed-header')
  }
}

$(window).scroll(function(event){
    didScroll = true;
    hasScrolled();
});

setInterval(function() {
    if (didScroll) {
      didScroll = false;
    }
}, 1);

function hasScrolled() {
    var st = $(this).scrollTop();

    // Make sure they scroll more than delta
    if(Math.abs(lastScrollTop - st) <= delta) {
      return;
    }
    // If they scrolled down and are past the navbar, add class .nav-up.
    // This is necessary so you never see what is "behind" the navbar.
    if (st > lastScrollTop && st > parseInt(navbarHeight * 2)){
        // Scroll Down
        header.removeClass('nav-down').addClass('nav-up');
    } else {
        // Scroll Up
        if(st + $(window).height() < $(document).height()) {
            header.removeClass('nav-up').addClass('nav-down');
        }
    }

    lastScrollTop = st;
};
