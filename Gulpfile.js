'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const concat = require('gulp-concat');
const cssnano = require('gulp-cssnano');
const browserSync = require('browser-sync').create();
const cleanCSS = require('gulp-clean-css');
const minifyInline = require('gulp-minify-inline');

const sassOptions = {
  errLogToConsole: true,
  outputStyle: 'expanded'
};

sass.compiler = require('node-sass');

const clean = function(cb) {
  cb();
};

// Static server
const serve = function(cb) {
  gulp.task('browser-sync', function() {
      browserSync.init({
          server: {
              baseDir: "./"
          }
      });
  });
  cb();
};

const css = gulp.series(clean, function(cb) {
  return gulp.src('./sass/main.scss')
    .pipe(sourcemaps.init())
    .pipe(sass(sassOptions).on('error', sass.logError))
    .pipe(cssnano())
    .pipe(cleanCSS({ keepSpecialComments : false }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./css'));
  cb();
});

const guide = gulp.series(clean, function(cb) {
  return gulp.src('./sass/guide.scss')
    .pipe(sourcemaps.init())
    .pipe(sass(sassOptions).on('error', sass.logError))
    .pipe(cssnano())
    .pipe(cleanCSS({ keepSpecialComments : false }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./css'));
  cb();
});

function watch() {
    browserSync.init({
        server: {
           baseDir: "./"
        }
    });
    gulp.watch('sass/**/*.scss', css)
    gulp.watch('sass/**/*.scss', guide)
    gulp.watch('./*.html').on('change',browserSync.reload);
    gulp.watch('./js/**/*.js').on('change', browserSync.reload);
}

exports.watch = watch;
